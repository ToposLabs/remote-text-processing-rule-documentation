# API contract with remote text processing rule
## REST API's
The API's described below is mandatory.
### 1. GET `/rule/info`  
Request param `locale` - IETF BCP 47 language tag.

This API should return information about rule, example:
```
{
  "name": "Remote rule",
  "description": "This is simple example of remote rule",
  "version": "1.3.0",
  "author": "Hronom",
  "includeContextFields": [
    "all"
  ],
  "dependencies": [
    "TextParser"
  ]
}
```

### 2. GET `/rule/config/schema`  
https://app.quicktype.io/#l=schema can be used to help generate schema.

To inform the UI an input widget is required you can specify the widget parameter in the schema. 
List of available widgets:
- topicEntity

example:
```
"nameId": {
    "type": "string",
    "description": "Lexeme name",
    "title": "Lexeme name",
    "type": "string",
    "widget": "topicEntity"
}
```

Request param `locale` - IETF BCP 47 language tag.
 
This API used for returning description of parameters.
```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "title": "Alpha Rule config",
  "type": "object",
  "additionalProperties": false,
  "description": "alpha-rule-config.schema.description",
  "properties": {
    "entries": {
      "type": "array",
      "items": {
        "$ref": "#/definitions/Entry"
      },
      "description": "alpha-rule-config.entries",
      "title": "Entries"
    }
  },
  "definitions": {
    "Entry": {
      "type": "object",
      "additionalProperties": false,
      "description": "entry.schema.description",
      "properties": {
        "name": {
          "type": "string",
          "description": "entry.name",
          "title": "Name"
        },
        "tags": {
          "type": "array",
          "items": {
            "type": "string"
          },
          "description": "entry.tags",
          "title": "Tags"
        },
        "subEntries": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/SubEntry"
          },
          "description": "entry.subEntries",
          "title": "Sub entries"
        }
      }
    },
    "SubEntry": {
      "type": "object",
      "additionalProperties": false,
      "description": "sub-entry.schema.description",
      "properties": {
        "name": {
          "type": "string",
          "description": "sub-entry.name",
          "title": "Name"
        },
        "value": {
          "type": "number",
          "minimum": 0.0,
          "maximum": 1.0,
          "default": "0.5",
          "description": "sub-entry.value",
          "title": "Value"
        }
      }
    }
  }
}
```

this schema is for next JSON object:
```
{
  "entries": [
    {
      "name": "Test property",
      "tags": [
        "Test1",
        "Test2",
        "Test3"
      ],
      "subEntries": [
        {
          "name": "Test sub entry 1",
          "value": 0.0
        },
        {
          "name": "Test sub entry 2",
          "value": 0.5
        },
        {
          "name": "Test sub entry 3",
          "value": 1.0
        }
      ]
    }
  ]
}
```

### 3. POST `/rule/config/schema`  
This endpoint allows to validate config.

Request:
```
{
  "entries": [
    {
      "name": "Test property",
      "tags": [
        "Test1",
        "Test2",
        "Test3"
      ],
      "subEntries": [
        {
          "name": "Test sub entry 1",
          "value": 0.0
        },
        {
          "name": "Test sub entry 2",
          "value": 0.5
        },
        {
          "name": "Test sub entry 3",
          "value": 1.0
        }
      ]
    }
  ]
}
```

Response if ok:
* Response headers: `content-type: application/json;charset=UTF-8`
* Response body: `true`

Response if not ok:
* Response headers: `content-type: application/json;charset=UTF-8`
* Response body: `false`

### 4. GET `/rule/config`  
This endpoint should return default config.

Example response:
```
{
  "entries": [
    {
      "name": "Test property",
      "tags": [
        "Test1",
        "Test2",
        "Test3"
      ],
      "subEntries": [
        {
          "name": "Test sub entry 1",
          "value": 0.0
        },
        {
          "name": "Test sub entry 2",
          "value": 0.5
        },
        {
          "name": "Test sub entry 3",
          "value": 1.0
        }
      ]
    }
  ]
}
```

### 5. POST `/rule/process`  
This API used for processing text.
Request example:
```
{
  "text": "Hello world Odessa Ukraine",
  "language": "en",
  "config": {
    "entries": [
      {
        "name": "Test property",
        "tags": [
          "Test1",
          "Test2",
          "Test3"
        ],
        "subEntries": [
          {
            "name": "Test sub entry 1",
            "value": 0.0
          },
          {
            "name": "Test sub entry 2",
            "value": 0.5
          },
          {
            "name": "Test sub entry 3",
            "value": 1.0
          }
        ]
      }
    ]
  },
  "context": {
    "language": {
      "id": "F760F66066650AB540C48A08194823CABD7C27DE1160FBA235147F362998B39ACDCCB7AE21B08B7245640F0639E8436ADD1C477E31001B4255B49FD64938D33A",
      "name": "English",
      "code": "en",
      "tag": "en-GB"
    },
    "timeIndex": "NaN",
    "tenseIndexes": [
      0.0,
      0.0,
      0.0
    ],
    "sentimentIndex": 0.500891111628079,
    "datelineCandidate": null,
    "dictionaryProximities": [
      {
        "topicId": "178E169D84B4E251B14D60E898DABA2A09F3B75792805723A2D4FEB8AD5810084441324EC1506B92A584EFE6D908A38A3DFCA79ED1E0FBA2B5547F36A9D833DA",
        "topicDictionaryName": "Law and Crime",
        "proximity": 0.1182844860642371,
        "topicTypeProximities": [
          {
            "topicTypeId": "8C038305130844D02EDA9451034478D0A8637CE61872EDBC4E2C76243F8AA680DA978DB62EA286604A76110E42933811A6673C054A7B60392ECFE4AD3243A841",
            "topicTypeName": "Criminal Activities",
            "proximity": 0.12106554100112041
          },
          {
            "topicTypeId": "EF60E066706B27B34DB9F73260271BB3CB001F857B118EDF2D58025C43EFD9EBA6B9AFB639A8936A5D7C171E21F05B72C5045F662918035A4DAC87CE5120CB22",
            "topicTypeName": "Terrorism",
            "proximity": 0.11473768659516662
          },
          {
            "topicTypeId": "E966E660766D21B54BBFF13466211DB5CD0619837D1788D92B4913415AEFC3E5BFF2FADB56C2F91F5B7A111827F65D74C30259602F1E055C4BAA81C85726CD24",
            "topicTypeName": "Criminal Skills",
            "proximity": 0.0986385192251489
          }
        ]
      },
      {
        "topicId": "881189021B2B7DCE2ED2FF77074525B5966C28C80D1FC5AF3E4B612732C7898BC6D6BAA53FA69A605F75047946973C15A26338014E7F643D2ACBE0A93647AC45",
        "topicDictionaryName": "Art and Entertainment",
        "proximity": 0.11360449762161891,
        "topicTypeProximities": [
          {
            "topicTypeId": "A42BAB2D3B206CF806F2BC74386F50F8804B54C82C47CD831226450B17AE8EBD9EDE968901C3B753367A29260FCE7D4AAE20660D363B2D707282BEF61A6B8069",
            "topicTypeName": "Arts or Museums or Theaters",
            "proximity": 0.11876956976728731
          },
          {
            "topicTypeId": "75FA7AFCEAF1BD29D7236DA5E9BE8129519A8519FD961C52C3F794DAC67F5F6C4F0B5B58C6407D91AE88E0E1D51EE1BE3AF0B099C0A2F8AEB3165822AED425CB",
            "topicTypeName": "Entertainment Venues and Events",
            "proximity": 0.11869292605979419
          },
          {
            "topicTypeId": "A42BAB2D3B206CF806F2BC74386F50F8804B54C82C47CD831226450B17AE8EBD9ED791901D91D82116375C556ABB10398E4F142D6253481106E7CC851A6B8069",
            "topicTypeName": "Humor",
            "proximity": 0.10271689028488576
          }
        ]
      },
      {
        "topicId": "C65FC74C55653380609CB139490B6BFBD822668643519DF276432F757D89AADC999CF6F07EF2934374553E3708D9725BEC2D764F00312A736485AEE77809E20B",
        "topicDictionaryName": "Warfare (weapons)",
        "proximity": 0.11205419976589079,
        "topicTypeProximities": [
          {
            "topicTypeId": "22AD2DABBDA6EA7E80743AE4ADEF907E1ACCD223B3D04F078FAFD9CAFC000F23716D03098D655EA790B1DAD3EC3D96BF08C992ABE4D5CE9780614A039CED06EF",
            "topicTypeName": "Military",
            "proximity": 0.11188056648293204
          },
          {
            "topicTypeId": "109F1F998F94D84CB24608D69FDDA24C28FEE01181E27D35BD9DEBF8CE393D0F4F4A2224B5576C95A283E8E1DE0FA48D3AFBA099D6E7FCA5B2537831AEDF34DD",
            "topicTypeName": "Firearms",
            "proximity": 0.07601640666679736
          },
          {
            "topicTypeId": "9D129214021955C13FCB855B12502FC1A5736D9C0C6FF0B83010667543BFAC9ECED2B4AB25A9E1182F0E656C53822900B7762D145B6A71283FDEF5BC2352B950",
            "topicTypeName": "Munitions",
            "proximity": 0.0625415652545155
          }
        ]
      }
    ],
    "themeProximities": [],
    "doctypeProximities": [],
    "emotionProximities": [],
    "geoEntities": [
      {
        "dictionaryId": "42DB43C8D1E1B704E41835BDCD8FEF7F4FACFD6BF1944E1780A1CA839C2D062F7879021B94053EC7F0D1BAB38C5DF6DF68A9F2CB84B5AEF7E0012A63FC8D668F",
        "dictionaryName": "Ukraine",
        "entityId": "EF76EE657C4D1DBE54A88E7E463554D2F0253FAE7D0C93800223145942AECCE7BABACEDB5CDBBD052F1B382818C06C4BF42B5F662918035A4DAC87CE5120CB22",
        "mainName": "Ukraine",
        "weblink": "http://en.wikipedia.org/wiki/Ukrainian_Soviet_Socialist_Republic",
        "relevance": 0.6212108998562811,
        "sentimentScore": 0.3111590181844919,
        "lexemePositions": [
          {
            "offset": 19,
            "length": 7
          }
        ],
        "location": {
          "lat": 49.0,
          "lon": 32.0
        },
        "population": 45415596,
        "feature": "A.PCLI",
        "parents": [
          {
            "featureCode": "independent political entity",
            "location": "Ukraine"
          }
        ]
      },
      {
        "dictionaryId": "42DB43C8D1E1B704E41835BDCD8FEF7F4FACFD6BF1944E1780A1CA839C2D062F7879021B94053EC7F0D1BAB38C5DF6DF68A9F2CB84B5AEF7E0012A63FC8D668F",
        "dictionaryName": "Ukraine",
        "entityId": "F861F9726B5A0AA943BF9969512243C5E73228B96A1B84971534034E55B9DBF0ADADD9CC4BCCAA12380C2F3F0FDF7B56EA3C48713E0F144D5ABB90D94637DC35",
        "mainName": "Odes'ka Oblast'",
        "weblink": "http://en.wikipedia.org/wiki/Odessa_Oblast",
        "relevance": 0.5823293904058778,
        "sentimentScore": 0.2916836156941017,
        "lexemePositions": [
          {
            "offset": 12,
            "length": 6
          }
        ],
        "location": {
          "lat": 46.75,
          "lon": 30.25
        },
        "population": 2687543,
        "feature": "A.ADM1",
        "parents": [
          {
            "featureCode": "independent political entity",
            "location": "Ukraine"
          },
          {
            "featureCode": "first-order administrative division",
            "location": "Odes'ka Oblast'"
          }
        ]
      },
      {
        "dictionaryId": "42DB43C8D1E1B704E41835BDCD8FEF7F4FACFD6BF1944E1780A1CA839C2D062F7879021B94053EC7F0D1BAB38C5DF6DF68A9F2CB84B5AEF7E0012A63FC8D668F",
        "dictionaryName": "Ukraine",
        "entityId": "F168F07B625303A04AB69060582B4ACCEE3B21B063128D9E1C3D0A475CB0D2F9A4A4D0C542C5A31B3105263606D67258EB35417837061D4453B299D04F3ED53C",
        "mainName": "Odessa",
        "weblink": "http://en.wikipedia.org/wiki/Odessa",
        "relevance": 0.5670222636886202,
        "sentimentScore": 0.28401641197686267,
        "lexemePositions": [
          {
            "offset": 12,
            "length": 6
          }
        ],
        "location": {
          "lat": 46.47747,
          "lon": 30.73262
        },
        "population": 1001558,
        "feature": "P.PPLA",
        "parents": [
          {
            "featureCode": "independent political entity",
            "location": "Ukraine"
          },
          {
            "featureCode": "first-order administrative division",
            "location": "Odes'ka Oblast'"
          },
          {
            "featureCode": "seat of a first-order administrative division",
            "location": "Odessa"
          }
        ]
      },
      {
        "dictionaryId": "54CD55DEC7F7A112F20E23ABDB99F96959BAEB7DE790580196B7DC958A3B10396E6F140D821328D1E6C7ACA59A4BE0C97EBFE4DD92A3B8E1F6173C75EA9B7099",
        "dictionaryName": "United States",
        "entityId": "32AB33B8A190C063897553A39BE8890F2DEAE273A0D14E5DDFFEC9849F73113A67671306810660D8F2C6E5F6C91FB19A2DEDADBBF4C5DE8790715A138CFD16FF",
        "mainName": "Odessa",
        "weblink": "http://en.wikipedia.org/wiki/Odessa%2C_Texas",
        "relevance": 0.36225778640981354,
        "sentimentScore": 0.1814517053307387,
        "lexemePositions": [
          {
            "offset": 12,
            "length": 6
          }
        ],
        "location": {
          "lat": 31.84568,
          "lon": -102.36764
        },
        "population": 99940,
        "feature": "P.PPLA2",
        "parents": [
          {
            "featureCode": "independent political entity",
            "location": "United States"
          },
          {
            "featureCode": "first-order administrative division",
            "location": "Texas"
          },
          {
            "featureCode": "seat of a second-order administrative division",
            "location": "Odessa"
          }
        ]
      }
    ],
    "topicEntities": [],
    "annotationSet": {
      "textRandomString": [
        {
          "start": 0,
          "end": 26,
          "text": "Hello world Odessa Ukraine",
          "features": {
            "generatedString_keyword": "Qgbuy gxiny Gszump Uakhbjh",
            "textSize_integer": 26
          }
        }
      ]
    },
    "logging": []
  }
}
```

Response example:
```
{
  "annotationSet": {
    "nameOfTheAnnotationsSet": [
      {
        "text": "Hello",
        "start": 0,
        "end": 6,
        "features": {
          "beginsFromUpper_boolean": true
        }
      },
      {
        "text": "world",
        "start": 7,
        "end": 12,
        "features": {
          "beginsFromUpper_boolean": false
        }
      },
      {
        "text": "Odessa",
        "start": 13,
        "end": 19,
        "features": {
          "beginsFromUpper_boolean": true
        }
      },
      {
        "text": "Ukraine",
        "start": 20,
        "end": 27,
        "features": {
          "beginsFromUpper_boolean": true
        }
      }
    ]
  }
}
```

To make results be a properly parsed in Elasticsearch - you need to add suffix to the name of the "feature".  
This suffix will be used to detect type of value.

#### List of possible suffixes:
* `_keyword` - keyword
* `_text` - text
* `_long` - long
* `_integer` - integer
* `_double` - double
* `_float` - float
* `_date` - date
* `_boolean` - boolean
* `_binary` - binary
* `_object` - object
* `_nested` - nested object
* `_geo_point` - geo_point
* `_geo_shape` - geo_shape

## Register for discovery  
Rule should register itself for discovery in [Eureka](https://github.com/Netflix/eureka) service.  
When rule registering it should add meta field called `type` and value `text-processing-rule`, so it should looks like:
```
type=text-processing-rule
```

Also rule should maintain it's health status.

This mechanic will used for discovering and load balancing.